﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TCPSever
{
    class TokenGenerator
    {
        public string NextTokenNumber(List<string> DocsInRooms, int lastPatient)
        {
           
            string nxtToken = string.Empty;
            string nxtTokenFor = string.Empty;

            List<string> Docs = new List<string>();

            foreach (var room in DocsInRooms)
            {
                string rm = room.Substring(0, 1);
                int docs = int.Parse(room.Substring(1));

                for (int i = 1; i < docs + 1; i++)
                {
                    Docs.Add((rm + i.ToString()));
                }
            }

            Docs.Sort();

            int CompletedRounds = lastPatient / Docs.Count;
            int CurIndex = (lastPatient % Docs.Count) + 1;

            string CurRoom = Docs[CurIndex - 1].Substring(0, 1);
            int VolCurRoom = 0;

            foreach (var room in DocsInRooms)
            {
                if (CurRoom == room.Substring(0, 1))
                {
                    VolCurRoom = int.Parse(room.Substring(1));
                    break;
                }
            }

            int PatientsInCompletedRounds = CompletedRounds * VolCurRoom;

            //nxtToken = CurRoom + string.Format("{0:000}", int.Parse(Docs[CurIndex - 1].Substring(1)));
            nxtToken = CurRoom + string.Format("{0:000}", PatientsInCompletedRounds + int.Parse(Docs[CurIndex - 1].Substring(1)));
           
            return nxtToken;
            

        }

       
    }
}
