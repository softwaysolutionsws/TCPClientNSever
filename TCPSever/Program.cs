﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace TCPSever
{
    class Program
    {
        private static byte[] _buffer = new byte[1024];
        private static List<Socket> _clientSocket = new List<Socket>();
        private static Socket _seversocket = new Socket(AddressFamily.InterNetwork,SocketType.Stream, ProtocolType.Tcp);
       private static int Count = 0;
        static void Main(string[] args)
        {
            Console.Title = "server";
            SetupSever();
            Console.ReadLine();
        }
        private static void SetupSever()
        {
            Console.WriteLine("seting up sever ....");
            _seversocket.Bind(new IPEndPoint(IPAddress.Any, 100));
            _seversocket.Listen(1);
            _seversocket.BeginAccept(new AsyncCallback(AcceptCallback), null);

        }

        private static void AcceptCallback(IAsyncResult AR)
        {
            Socket socket = _seversocket.EndAccept(AR);
            _clientSocket.Add(socket);
            Console.WriteLine("Client Connected ....");
            socket.BeginReceive(_buffer, 0, _buffer.Length, SocketFlags.None, new AsyncCallback(ReciveCallback), socket);
            _seversocket.BeginAccept(new AsyncCallback(AcceptCallback), null);

        }
        private static void ReciveCallback(IAsyncResult AR)
        {
            Socket socket = (Socket)AR.AsyncState;
            int received = socket.EndReceive(AR);
            byte[] dataBuf = new byte[received];
            Array.Copy(_buffer, dataBuf, received);

            string text = Encoding.ASCII.GetString(dataBuf);

            string comName = text.Substring(0, 4);
            string Rvmsg = text.Substring(4, text.Length - 4);



           // Console.WriteLine(">>"+comName + " Send :  " + Rvmsg);


            string response = string.Empty;

            if (Rvmsg.ToLower() != "get token")
            {
                response = "Invalid Request";
            }
            else
            {
                response = DateTime.Now.ToLongTimeString();
                List<string> DocsInRooms = new List<string>
            {
                "A1","B1","C1","D1","E1","F1","G1"
            };
                TokenGenerator tokGen = new TokenGenerator();
                
                response = tokGen.NextTokenNumber(DocsInRooms, Count);
                Console.WriteLine(">>" + comName + " Send :-  " + response);
                Count++;

            }
            byte[] data = Encoding.ASCII.GetBytes(response);
            socket.BeginSend(data, 0, data.Length, SocketFlags.None, new AsyncCallback(SendCallback), socket);
            socket.BeginReceive(_buffer, 0, _buffer.Length, SocketFlags.None, new AsyncCallback(ReciveCallback), socket);
        }

        
        private static void SendCallback(IAsyncResult AR)
        {
            Socket socket = (Socket)AR.AsyncState;
            socket.EndSend(AR);
        }


    }
}
