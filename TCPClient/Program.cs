﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace TCPClient
{
    class Program
    {
        private static Socket _clientsockrt = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        public static string ComName = string.Empty;
        static void Main(string[] args)
        {
            Console.Title = "Client";
            Console.WriteLine("enter Com Name: As Like [com1,com2]");
            ComName = Console.ReadLine().ToString();
           
            LoopConnect();
            SendLoop();
            
        }

        private static void SendLoop()
        {
            while(true)
            {
                Console.WriteLine("Enter a request:");
                string req = Console.ReadLine() ;
                byte[] buffer = Encoding.ASCII.GetBytes(ComName+req);
                _clientsockrt.Send(buffer);

                byte[] recivedbuf = new byte[1024];
                int rec = _clientsockrt.Receive(recivedbuf);
                byte [] data =  new byte[rec];
                Array.Copy(recivedbuf, data, rec);
                Console.WriteLine("Received "+ Encoding.ASCII.GetString(data));
            }
            
        }

        private static void LoopConnect()
        {
            int attempts = 0;
            while(!_clientsockrt.Connected)
            {
                try
                {
                    attempts++;
                    _clientsockrt.Connect(IPAddress.Loopback, 100);
                }
                catch (SocketException)
                {
                    Console.Clear();
                    Console.WriteLine("connection attempts: "+ attempts.ToString());
                }
            }
              Console.Clear();
            Console.WriteLine("Connected");
            
        }
    }
}
